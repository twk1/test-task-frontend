export interface Article {
  id: number;
  title: string;
  url: string;
  short_description: string;
  created_at: string;
}

export interface FullArticle extends Article{
  full_description: string;
  is_show: boolean;
}